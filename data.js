const formulae = [
    {
        name: "密度公式",
        latex: "\\rho = {m \\over v}",
        special: null,
        category: "physics"
    },
    {
        name: "欧姆定律",
        latex: "I = {U \\over R}",
        special: null,
        category: "electron"
    },
    {
        name: "重力公式",
        latex: "G = mg",
        special: null,
        category: "physics"
    }
]

