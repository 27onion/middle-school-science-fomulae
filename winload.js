// import formulae from './data'

function drawDatas(formulaeData) {
    let domArea = document.getElementById("cards")
    if(domArea) {
        domArea.innerHTML = ``
        for(i of formulaeData) {
            domArea.innerHTML += `<div class="card"><span class="card_title">${i.name}</span>\n\$\$ ${i.latex} \$\$<br>${i.special === null ? "" : i.special}`
        }
        MathJax.typeset()
    } else {
        return
    }
}

window.onload = function() {
    // Logic on load.
    drawDatas(formulae)
}
